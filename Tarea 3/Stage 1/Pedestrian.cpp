#include <QtMath> // for M_PI and functions, see https://doc.qt.io/qt-5/qtmath.html
#include <string>
#include "Comuna.h"
#include "Pedestrian.h"

Pedestrian::Pedestrian (Comuna *com, double speed, double deltaAngle) /*....*/{
    myRand = QRandomGenerator::securelySeeded();
    this->speed = speed*(0.9+0.2*myRand.generateDouble());
    this->comuna = com;
    this->angle = myRand.generateDouble() * 2 * M_PI;
    this->deltaAngle = deltaAngle;
    x = 0;
    y = 0;
    x_tPlusDelta = 0;
    y_tPlusDelta = 0;
}
string Pedestrian::getState() const {
    string s = to_string(x) + ",\t";
    s += to_string(y);
    return s;
}
void Pedestrian::computeNextState(double delta_t) {
    double r = myRand.generateDouble(); // r entre 0 y 1
    double r2 = myRand.generateDouble(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
    if (r2 >= 0.5) {
        angle += r * deltaAngle; // cambia el angulo con delta positivo
    } else {
        angle -= r * deltaAngle; // cambia el angulo con delta negativo
    }
    double time;
    double time2;
    double angle2 = angle;

    if ((x + speed * delta_t * qCos(angle)) < 0) {
        time = x / (speed * (qAbs(qCos(angle))));
        time2 = delta_t - time;
        x_tPlusDelta = x + speed * time * (qCos(angle));
        angle = (3 * M_PI) - angle;
        x_tPlusDelta = x_tPlusDelta + speed * time2 * (qCos(angle));
    } else if ((x + speed * delta_t * qCos(angle)) > comuna->getWidth()) {
        time = (comuna->getWidth() - x) / (speed * (qAbs(qCos(angle))));
        time2 = delta_t - time;
        x_tPlusDelta = x + speed * time * (qCos(angle));
        angle = (3 * M_PI) - angle;
        x_tPlusDelta = x_tPlusDelta + speed * time2 * (qCos(angle));
    } else {
        x_tPlusDelta = x + speed * delta_t * qCos(angle);
    }

    if ((y + speed * delta_t * qSin(angle2)) < comuna->getHeight()
            && (y + speed * delta_t * qSin(angle2)) > 0) {
        y_tPlusDelta = y + speed * delta_t * qSin(angle2);

    } else if (y + speed * delta_t * qSin(angle2) > comuna->getHeight()) {

        time = (comuna->getHeight() - y) / (speed * (qAbs(qSin(angle2))));
        time2 = delta_t - time;
        y_tPlusDelta = y + speed * time * qSin(angle2);
        angle2 = (2 * M_PI) - angle2;
        y_tPlusDelta = y_tPlusDelta + (speed * time2 * (qSin(angle2)));
        angle = angle2;

    } else if (y + speed * delta_t * qSin(angle2) < 0) {
        time = y / (speed * (qAbs(qSin(angle2))));
        time2 = delta_t - time;
        y_tPlusDelta = y + speed * time * qSin(angle2);
        angle2 = (2 * M_PI) - angle2;
        y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((qSin(angle2))));
        angle = angle2;
    }
}
void Pedestrian::updateState(){
    x=x_tPlusDelta;
    y=y_tPlusDelta;
}
void Pedestrian::SetRandLoc(){
    x = myRand.generateDouble()* comuna->getWidth();
    y = myRand.generateDouble()* comuna->getHeight();
}
