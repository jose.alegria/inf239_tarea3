#include "Comuna.h"
Comuna::Comuna(double width, double length): territory(0,0,width,length){
    pPerson=NULL;
};
double Comuna::getWidth() const {
    return territory.width();
};
double Comuna::getHeight() const {
    return territory.height();
};
void Comuna::setPerson(Pedestrian *person){
    person->SetRandLoc();
    pPerson = person;
};
void Comuna::computeNextState (double delta_t) {
   pPerson->computeNextState(delta_t);
};
void Comuna::updateState () {
   pPerson->updateState();
};
string Comuna::getStateDescription(){
    return "t, \tx, \ty";
};
string Comuna::getState() const{
    return pPerson->getState();
};
