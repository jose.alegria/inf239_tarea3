#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QWidget* nWidget = new QWidget(NULL);
    QHBoxLayout* nLayout = new QHBoxLayout();
    QLabel* nLabel = new QLabel("N");
    nLayout->addWidget(nLabel);
    QLineEdit *txtfield1 = new QLineEdit();
    connect(txtfield1, SIGNAL(textChanged(QString)) ,this , SLOT(setPeople(QString)));
    nLayout->addWidget(txtfield1);
    nWidget->setLayout(nLayout);
    QWidgetAction *action0 = new QWidgetAction(ui->menuSettings);
    action0->setDefaultWidget(nWidget);
    ui->menuSettings->addAction(action0);

    QWidget* iWidget = new QWidget(NULL);
    QHBoxLayout* iLayout = new QHBoxLayout();
    QLabel* iLabel = new QLabel("I");
    iLayout->addWidget(iLabel);
    QLineEdit *txtfield2 = new QLineEdit();
    connect(txtfield2, SIGNAL(textChanged(QString)), this, SLOT(setInf(QString)));
    iLayout->addWidget(txtfield2);
    iWidget->setLayout(iLayout);
    QWidgetAction *action1 = new QWidgetAction(ui->menuSettings);
    action1->setDefaultWidget(iWidget);
    ui->menuSettings->addAction(action1);

    QWidget* tWidget = new QWidget(NULL);
    QHBoxLayout* tLayout = new QHBoxLayout();
    QLabel* tLabel = new QLabel("I Time");
    tLayout->addWidget(tLabel);
    QLineEdit *txtfield3 = new QLineEdit();
    connect(txtfield3, SIGNAL(textChanged(QString)), this, SLOT(setInfTime(QString)));
    tLayout->addWidget(txtfield3);
    tWidget->setLayout(tLayout);
    QWidgetAction *action2 = new QWidgetAction(ui->menuSettings);
    action2->setDefaultWidget(tWidget);
    ui->menuSettings->addAction(action2);

}

MainWindow::~MainWindow()
{
    delete ui;

}

void MainWindow::setGraph(){
    QChart *chart = new QChart();
    this->chart = chart;
    this->simu->chart = chart;
    QChartView *chartView = new QChartView(chart);
    chartView->setParent(ui->horizontalWidget);
}
void MainWindow::setSim(Simulator* sim){
    simu = sim;
}
void MainWindow::startSim(){
    simu->startSimulation();
    ui->menuSettings->setDisabled(true);
}
void MainWindow::stopSim(){
    simu->stopSimulation();
    ui->menuSettings->setDisabled(false);
}
void MainWindow::setPeople(QString field){
    int num = field.toInt();
    simu->getComuna()->setNumberOfPeople(num);
}
void MainWindow::setInf(QString field){
    int num = field.toInt();
    simu->getComuna()->setNumberOfInfected(num);
}
void MainWindow::setInfTime(QString field){
    double num = field.toDouble();
    simu->setInfTime(num);
}
/*
chart->setTitle("Comuna Covid");
chart->createDefaultAxes();
chart->axes(Qt::Horizontal).first()->setRange(0, 100);
chart->axes(Qt::Vertical).first()->setRange(0, 100);
chartView->setRenderHint(QPainter::Antialiasing);
chart->setAnimationOptions(QChart::AllAnimations);*/


