#include "Vacunatorio.h"
#include "Comuna.h"
Vacunatorio::Vacunatorio(double size, Comuna* lugar){
    myRand = QRandomGenerator::securelySeeded();
    comuna = lugar;
    this->size = size;
    AreaVac = new QRectF(myRand.generateDouble()*(comuna->getWidth()-size), myRand.generateDouble()*(comuna->getHeight()-size), size, size);
}
void Vacunatorio::setAnotherLoc(){
    AreaVac->setRect(myRand.generateDouble()*(comuna->getWidth()-size), myRand.generateDouble()*(comuna->getHeight()-size), size, size);
}
bool Vacunatorio::overlap(Vacunatorio* r){
    return AreaVac->x() < r->getX() + r->getWidth() && AreaVac->x() + AreaVac->width() > r->getX() && AreaVac->y() < r->getY() + r->getHeight() && AreaVac->y() + AreaVac->height() > r->getY();
}
double Vacunatorio::getX() const{
    return AreaVac->x();
}
double Vacunatorio::getY() const{
    return AreaVac->y();
}
double Vacunatorio::getWidth() const{
    return AreaVac->width();
}
double Vacunatorio::getHeight() const{
    return AreaVac->height();
}
QRectF* Vacunatorio::getArea() const{
    return AreaVac;
}
