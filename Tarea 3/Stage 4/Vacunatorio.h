#ifndef VACUNATORIO_H
#define VACUNATORIO_H
#include "Pedestrian.h"
#include "QRectF"
using namespace std;
class Comuna;
class Vacunatorio{
private:
    QRandomGenerator myRand;
    QRectF *AreaVac;
    Comuna *comuna;
    double size;

public:
    Vacunatorio(double size, Comuna* lugar);
    void setAnotherLoc();
    bool overlap(Vacunatorio* r);
    double getX() const;
    double getY() const;
    double getWidth() const;
    double getHeight() const;
    QRectF* getArea() const;
};

#endif // VACUNATORIO_H
