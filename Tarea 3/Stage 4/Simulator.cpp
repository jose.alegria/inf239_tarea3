#include "Simulator.h"
Simulator::Simulator(ostream *output, Comuna *com,double speed, double deltaAngle,double M,
                     double delta, double st, double d, double p0,double p1, double p2, double tiempoInfeccion,double timeActVacs, ofstream *Results){
    t=0;
    delta_t=delta;
    samplingTime=st;
    this->speed = speed;
    this->deltaAngle = deltaAngle;
    this->M = M;
    distanceI = d;
    prob0 = p0;
    prob1 = p1;
    prob2 = p2;
    tInfection = tiempoInfeccion;
    tVacs = timeActVacs;
    MyFile = Results;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(simulateSlot()));
    comuna = com;
    out = output;
}
Simulator::~Simulator(){
    delete timer;
}
void Simulator::printStateDescription() const {
    string s= comuna->getStateDescription();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::printState(double t) const{
    string s = to_string(t) + ",\t";
    s += comuna->getState();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::startSimulation(){
    comuna->Reset(speed, deltaAngle, M);
    printStateDescription();
    t=0;
    printState(t);
    timer->start(1000);
    chart->removeAllSeries();
    Simulator::createSeries();
}
void Simulator::stopSimulation(){
    timer->stop();
}
void Simulator::simulateSlot(){
    double nextStop=t+samplingTime;
    while(t<nextStop) {
       comuna->computeNextState(delta_t,distanceI,prob0,prob1,prob2,tInfection); // compute its next state based on current global state
       comuna->updateState();  // update its state
       Simulator::updateChart();
       comuna->setVacs(tVacs,t);
       t+=delta_t;
    }
    printState(t);
}
Simulator* Simulator::getSim(){
    return this;
}

double Simulator::getTime(){
    return t;
}
Comuna * Simulator::getComuna(){
    return comuna;
}
void Simulator::createSeries(){
    series1 = new QLineSeries();
    series2 = new QLineSeries();
    series3 = new QLineSeries();
    series4 = new QLineSeries();
    series5 = new QLineSeries();

    QColor* color = new QColor(94,70,36);
    areaSeriesVac = new QAreaSeries(series1,series2);
    areaSeriesVac->setName("Vac");
    areaSeriesVac->setColor(Qt::green);
    areaSeriesInf = new QAreaSeries(series2, series3);
    areaSeriesInf->setName("Inf");
    areaSeriesInf->setColor(Qt::red);
    areaSeriesRec = new QAreaSeries(series3,series4);
    areaSeriesRec->setName("Rec");
    areaSeriesRec->setColor(*color);
    areaSeriesSus = new QAreaSeries(series4, series5);
    areaSeriesSus->setName("Sus");
    areaSeriesSus->setColor(Qt::blue);

    chart->addSeries(areaSeriesVac);
    chart->addSeries(areaSeriesInf);
    chart->addSeries(areaSeriesRec);
    chart->addSeries(areaSeriesSus);

    chart->setTitle("Comuna CoVid");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).first()->setRange(0, 100);
    chart->axes(Qt::Vertical).first()->setRange(0, comuna->getNumberOfPeople());
    chart->setAnimationOptions(QChart::SeriesAnimations);
}
void Simulator::updateChart(){
    *series1 << QPointF(t,0);
    *series2 << QPointF(t,comuna->getVaccinated());
    *series3 << QPointF(t,comuna->getVaccinated()+comuna->getInf());
    *series4 << QPointF(t,comuna->getVaccinated()+comuna->getInf()+comuna->getRec());
    *series5 << QPointF(t,comuna->getVaccinated()+comuna->getInf()+comuna->getRec()+comuna->getSus());
    if(t>100){
        chart->axes(Qt::Horizontal).first()->setRange(0,t);
    } //V I R S
}
void Simulator::setInfTime(double num){
    tInfection = num;
}
