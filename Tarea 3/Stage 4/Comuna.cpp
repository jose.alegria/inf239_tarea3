#include "Comuna.h"
Comuna::Comuna(double width, double length, double NoP, double NoI, double NoV, double sV): territory(0,0,width,length){
    vector<Pedestrian*>vPerson;
    NumberOfPeople = NoP;
    NumberOfInfected = NoI;
    S = NoP - NoI;
    I = NoI;
    R = 0;
    V = 0;
    NumberOfVacs = NoV;
    SizeOfVacs = sV;
    myRand = QRandomGenerator::securelySeeded();
}
double Comuna::getWidth() const {
    return territory.width();
}
double Comuna::getHeight() const {
    return territory.height();
}
void Comuna::setNumberOfPeople(int num){
    NumberOfPeople = num;
}
void Comuna::setNumberOfInfected(int num){
    NumberOfInfected = num;
}
void Comuna::setPerson(Pedestrian *person){
    person->SetRandLoc();
    vPerson.push_back(person);
}
void Comuna::setPeople(double speed, double deltaAngle){
    if(!(vPerson.empty())){
        vPerson.clear();
    }
    for(int i=0; i<NumberOfPeople;i++){
        if(i<NumberOfInfected){
            Pedestrian* person = new Pedestrian(this, speed, deltaAngle, "I");
            setPerson(person);
        }else{
            Pedestrian* person = new Pedestrian(this, speed, deltaAngle, "S");
            setPerson(person);
        }
    }
}
void Comuna::setVacs(double timeActVac, double simTime){
    if(vVacs.empty()){
        if(simTime>timeActVac){
            for(int i=0; i<NumberOfVacs; i++){
                Vacunatorio* vacunatorio = new Vacunatorio(SizeOfVacs, this);
                if(i>0){
                    for(int j=0; j<i; j++){
                        while(vVacs.at(j)->overlap(vacunatorio)){
                            vacunatorio->setAnotherLoc();
                        }
                    }
                }
                vVacs.push_back(vacunatorio);
            }
        }
    }
}
void Comuna::computeNextState(double delta_t, double d, double p0, double p1, double p2, double tiempoInfeccion) {
   int i;
   int length = vPerson.size();
   for(i=0;i<length;i++){
       vPerson.at(i)->computeNextState(delta_t,d,p0,p1,p2,tiempoInfeccion);
   }
}
void Comuna::updateState () {
    int i;
    int length = vPerson.size();
    for(i=0;i<length;i++){
        vPerson.at(i)->updateState();
    }
   S=0;
   I=0;
   R=0;
   V=0;
   int N = vPerson.size();
   for (int i = 0; i < N; i++) {
       if (vPerson.at(i)->getState() == "I") {
           I += 1;
       } else if ((vPerson.at(i)->getState()) == "S") {
           S += 1;
       } else if (vPerson.at(i)->getState() == "R") {
           R += 1;
       } else if (vPerson.at(i)->getState() == "V"){
           V += 1;
       }
   }

}
string Comuna::getStateDescription(){
    return "t, \t\tSus, \t\tInf, \t\tRec, \t\tVac";
}
string Comuna::getState() const{
    return to_string(S) + ", \t\t" + to_string(I) + ", \t\t" + to_string(R) + ", \t\t" + to_string(V);
}
vector<Pedestrian*> Comuna::getPeople() const{
    return vPerson;
}
int Comuna::getNumberOfPeople() const{
    return NumberOfPeople;
}
int Comuna::getSus() const{
    return S;
}
int Comuna::getInf() const{
    return I;
}
int Comuna::getRec() const{
    return R;
}
int Comuna::getVaccinated() const{
    return V;
}
vector<Vacunatorio*> Comuna::getVacs() const{
    return vVacs;
}
void Comuna::setMasksPeople(double M){
    int cant = (int)(vPerson.size() * M);
    for (int i =0; i < cant; i++){
        int rand = (int)(myRand.generateDouble()*vPerson.size());
        if(vPerson.at(rand)->getMask() == "ON"){
            i--;
        }else {
            vPerson.at(rand)->setMask();
        }
    }
}
void Comuna::Reset(double speed, double deltaAngle, double M){
   vVacs.clear();
   S = NumberOfPeople - NumberOfInfected;
   I = NumberOfInfected;
   R = 0;
   V = 0;
   Comuna::setPeople(speed, deltaAngle);
   Comuna::setMasksPeople(M);

}
