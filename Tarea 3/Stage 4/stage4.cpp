#include "Comuna.h"
#include "Simulator.h"
#include "Pedestrian.h"
#include "mainwindow.h"
#include <QCoreApplication>
#include <QApplication>
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if (argc != 2){
        cout << "Usage: stage1 <configurationFile.txt>" << endl;
        exit(-1);
    }
    ifstream fin(argv[1]);
    double N, I, I_time, comunaWidth,comunaLength,speed, delta_t,
            deltaAngle, distanceInf, ratioMasks, prob0, prob1, prob2,
            NumVac, VacSize, VacTime;
    // this is not really needed, just to check data read.
    cout << "File: " << argv[1] << endl;
    fin >> N >> I >> I_time;
    fin >> comunaWidth >> comunaLength;
    fin >> speed >> delta_t >> deltaAngle;
    fin >> distanceInf >> ratioMasks >> prob0 >> prob1 >> prob2;
    fin >> NumVac >> VacSize >> VacTime;
    cout << N << "," << I << "," << I_time << endl;
    cout << comunaWidth <<"," << comunaLength << endl;
    cout << speed <<","<< delta_t <<"," << deltaAngle<< endl;
    cout << distanceInf <<"," << ratioMasks <<","<< prob0 <<","<<prob1 <<","<<prob2 <<endl;
    cout << NumVac <<","<<VacSize<<","<<VacTime<<endl;
    double samplingTime = 1.0;
    ofstream MyFile("ResultsSim.txt");
    Comuna comuna(comunaWidth, comunaLength, N, I,NumVac,VacSize);
    comuna.setPeople(speed, deltaAngle);
    comuna.setMasksPeople(ratioMasks);
    Simulator sim(&cout, &comuna, speed, deltaAngle, ratioMasks, delta_t, samplingTime,distanceInf,prob0,prob1,prob2,I_time,VacTime, &MyFile);
    //sim.startSimulation();
    MainWindow w;
    w.setSim(sim.getSim());
    w.setGraph();
    w.show();
    return a.exec();
}
