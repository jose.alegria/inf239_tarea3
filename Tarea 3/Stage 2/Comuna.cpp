#include "Comuna.h"
Comuna::Comuna(double width, double length, double NoP, double NoI): territory(0,0,width,length){
    vector<Pedestrian*>vPerson;
    NumberOfPeople = NoP;
    NumberOfInfected = NoI;
}
double Comuna::getWidth() const {
    return territory.width();
}
double Comuna::getHeight() const {
    return territory.height();
}
void Comuna::setPerson(Pedestrian *person){
    person->SetRandLoc();
    vPerson.push_back(person);
}
void Comuna::setPeople(double speed, double deltaAngle){
    if(!(vPerson.empty())){
        vPerson.clear();
    }
    for(int i=0; i<NumberOfPeople;i++){
        if(i<NumberOfInfected){
            Pedestrian* person = new Pedestrian(this, speed, deltaAngle, "I");
            setPerson(person);
        }else{
            Pedestrian* person = new Pedestrian(this, speed, deltaAngle, "S");
            setPerson(person);
        }
    }
}
void Comuna::computeNextState(double delta_t, double d, double p0, double tiempoInfeccion) {
   int i;
   int length = vPerson.size();
   for(i=0;i<length;i++){
       vPerson.at(i)->computeNextState(delta_t,d,p0,tiempoInfeccion);
   }
}
void Comuna::updateState () {
    int i;
    int length = vPerson.size();
    for(i=0;i<length;i++){
        vPerson.at(i)->updateState();
    }
}
string Comuna::getStateDescription(){
    return "t, \tSus, \tInf, \tRec";
}
string Comuna::getState() const{
    int S=0;
    int I=0;
    int R=0;
    int N = vPerson.size();
    for (int i = 0; i < N; i++) {
        if (vPerson.at(i)->getState() == "I") {
            I += 1;
        } else if ((vPerson.at(i)->getState()) == "S") {
            S += 1;
        } else if (vPerson.at(i)->getState() == "R") {
            R += 1;
        } /*else if (vPerson.at(i)->getState() == "V"){
            V += 1;
        }*/
    }

    return to_string(S) + ", \t" + to_string(I) + ", \t" + to_string(R);
}
vector<Pedestrian*> Comuna::getPeople() const{
    return vPerson;
}
