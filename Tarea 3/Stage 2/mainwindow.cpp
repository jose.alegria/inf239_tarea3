#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QLineSeries *series1 = new QLineSeries();
    QLineSeries *series2 = new QLineSeries();
    QAreaSeries *seriesSus = new QAreaSeries(series1, series2);
    //QAreaSeries *seriesInf = new QAreaSeries(...
    //QAreaSeries *seriesRec = new QAreaSeries(...
    series1->append(0,2);
    series2->append(4,10);

    QChart *chart = new QChart();
    chart->addSeries(seriesSus);
    chart->setTitle("Comuna Covid");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).first()->setRange(0, 20);
    chart->axes(Qt::Vertical).first()->setRange(0, 10);
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setParent(ui->horizontalLayoutWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}
