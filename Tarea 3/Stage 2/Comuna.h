#ifndef COMUNA_H
#define COMUNA_H
#include "Pedestrian.h"
#include <QRect>
#include <string>
#include <vector>
using namespace std;
class Comuna {
private:
    vector<Pedestrian*> vPerson;
    QRect territory;
    double NumberOfPeople;
    double NumberOfInfected;

public:
    Comuna(double width, double length,double NoP, double NoI);
    double getWidth() const;
    double getHeight() const;
    void setPerson(Pedestrian *person);
    void setPeople(double speed, double deltaAngle);
    void computeNextState (double delta_t, double d, double p0, double tiempoInfeccion);
    void updateState ();
    static string getStateDescription();
    string getState() const;
    vector<Pedestrian*> getPeople() const;
 };


#endif // COMUNA_H
