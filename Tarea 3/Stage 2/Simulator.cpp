#include "Simulator.h"
Simulator::Simulator(ostream *output, Comuna *com,
                     double delta, double st, double d, double p0, double tiempoInfeccion, ofstream *Results){
    t=0;
    delta_t=delta;
    samplingTime=st;
    distanceI = d;
    prob0 = p0;
    tInfection = tiempoInfeccion;
    MyFile = Results;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(simulateSlot()));
    comuna = com;
    out = output;
}
Simulator::~Simulator(){
    delete timer;
}
void Simulator::printStateDescription() const {
    string s= comuna->getStateDescription();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::printState(double t) const{
    string s = to_string(t) + ",\t";
    s += comuna->getState();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::startSimulation(){
    printStateDescription();
    t=0;
    printState(t);
    timer->start(1000);
}
void Simulator::simulateSlot(){
    double nextStop=t+samplingTime;
    while(t<nextStop) {
       comuna->computeNextState(delta_t,distanceI,prob0,tInfection); // compute its next state based on current global state
       comuna->updateState();  // update its state
       t+=delta_t;
    }
    printState(t);
}
