#include <QtMath> // for M_PI and functions, see https://doc.qt.io/qt-5/qtmath.html
#include <string>
#include "Comuna.h"
#include "Pedestrian.h"

Pedestrian::Pedestrian (Comuna *com, double speed, double deltaAngle, string salud){
    myRand = QRandomGenerator::securelySeeded();
    this->speed = speed*(0.9+0.2*myRand.generateDouble());
    this->comuna = com;
    this->angle = myRand.generateDouble() * 2 * M_PI;
    this->deltaAngle = deltaAngle;
    this->estadoSalud = salud;
    this->estadoSalud_tPlusDelta = salud;
    timerRecuperacion = 0;
    this->x = 0;
    this->y = 0;
    x_tPlusDelta = 0;
    y_tPlusDelta = 0;
}
string Pedestrian::getState() const {
    return estadoSalud;
}
double Pedestrian::getX()const{
    return x;
}
double Pedestrian::getY()const{
    return y;
}

void Pedestrian::computeAngle(){
    double r = myRand.generateDouble(); // r entre 0 y 1
    double r2 = myRand.generateDouble(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
    if (r2 >= 0.5) {
        angle += r * deltaAngle; // cambia el angulo con delta positivo
    } else {
        angle -= r * deltaAngle; // cambia el angulo con delta negativo
    }
}

void Pedestrian::computeXY(double delta_t){
    double time;
    double time2;
    double angle2 = angle;

    if ((x + speed * delta_t * qCos(angle)) < 0) {
        time = x / (speed * (qAbs(qCos(angle))));
        time2 = delta_t - time;
        x_tPlusDelta = x + speed * time * (qCos(angle));
        angle = (3 * M_PI) - angle;
        x_tPlusDelta = x_tPlusDelta + speed * time2 * (qCos(angle));
    } else if ((x + speed * delta_t * qCos(angle)) > comuna->getWidth()) {
        time = (comuna->getWidth() - x) / (speed * (qAbs(qCos(angle))));
        time2 = delta_t - time;
        x_tPlusDelta = x + speed * time * (qCos(angle));
        angle = (3 * M_PI) - angle;
        x_tPlusDelta = x_tPlusDelta + speed * time2 * (qCos(angle));
    } else {
        x_tPlusDelta = x + speed * delta_t * qCos(angle);
    }

    if ((y + speed * delta_t * qSin(angle2)) < comuna->getHeight()
            && (y + speed * delta_t * qSin(angle2)) > 0) {
        y_tPlusDelta = y + speed * delta_t * qSin(angle2);

    } else if (y + speed * delta_t * qSin(angle2) > comuna->getHeight()) {

        time = (comuna->getHeight() - y) / (speed * (qAbs(qSin(angle2))));
        time2 = delta_t - time;
        y_tPlusDelta = y + speed * time * qSin(angle2);
        angle2 = (2 * M_PI) - angle2;
        y_tPlusDelta = y_tPlusDelta + (speed * time2 * (qSin(angle2)));
        angle = angle2;

    } else if (y + speed * delta_t * qSin(angle2) < 0) {
        time = y / (speed * (qAbs(qSin(angle2))));
        time2 = delta_t - time;
        y_tPlusDelta = y + speed * time * qSin(angle2);
        angle2 = (2 * M_PI) - angle2;
        y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((qSin(angle2))));
        angle = angle2;
    }
}
void Pedestrian::computeNextState(double delta_t, double d, double p0, double tiempoInfeccion) {
    if (estadoSalud == "S"){
        LookforPersonInf(comuna->getPeople(), d, p0);
    }
    else if (estadoSalud =="I") {
        startTimerRecovery(delta_t, tiempoInfeccion);
    }
    computeAngle();
    computeXY(delta_t);
}
void Pedestrian::LookforPersonInf(vector<Pedestrian*>vP, double d, double p0){
    for(unsigned int i=0; i< vP.size(); i++){
        double D = qSqrt((x-(vP.at(i)->getX()))*(x-(vP.at(i)->getX())) + (y-(vP.at(i)->getY()))*(y-(vP.at(i)->getY()))); //Si es que ciertas condiciones se cumplen entonces hay una probabilidad de que este se infecte.
        if (D <= d && estadoSalud == "S" && vP.at(i)->getState() == "I"){
            double r = myRand.generateDouble();
            if (r <= p0) {
                estadoSalud_tPlusDelta = "I";
            } // For es utilizado para ir viendo la distancia del individuo a cada uno de los otros en el array de la comuna.
        }
    }
}
void Pedestrian::startTimerRecovery(double delta_t, double tiempoInfeccion){
    timerRecuperacion += delta_t;
    if(timerRecuperacion>=tiempoInfeccion){
        estadoSalud_tPlusDelta = "R";
        timerRecuperacion = 0;
    } //Ocupa su propio timer para ver si suficiente tiempo ocurrio para que el nuevo estadoSalud sea "R")
}
void Pedestrian::updateState(){
    x=x_tPlusDelta;
    y=y_tPlusDelta;
    estadoSalud = estadoSalud_tPlusDelta;
}
void Pedestrian::SetRandLoc(){
    x = myRand.generateDouble()* comuna->getWidth();
    y = myRand.generateDouble()* comuna->getHeight();
}
