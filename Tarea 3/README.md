#### Approach: 
Esta tarea busca proyectar el comportamiento de una pandemia en un espacio confinado con múltiples variables a considerar a traves del lenguaje de programacion C++. Los archivos que la componen son:

- configurationFile.txt
- Comuna.cpp
- Comuna.h
- Pedestrian.cpp
- Pedestrian.h
- Simulator.cpp
- Simulator.h
- Vacunatorio.cpp
- Vacunatorio.h
- stage4.cpp
- Tarea_3.pro
- Tarea_3.pro.user

Se adjunta el archivo extra:
- diagram.qmodel

El programa también genera un archivo de texto con los resultados de la evolucion de la Pandemia:
- ResultsSim.txt


#### Instrucciones de compilación en Qt Creator 5.12.11:
1. Abrir Qt Creator.
2. Con la configuración mingw apropiada seleccionada (32-bit o 64-bit, dependiendo de la maquina), ingresar a la pestaña "Welcome" a la izquierda de la ventana.
3. Ingresar al tab "Welcome>Projects".
4. Click "Welcome>Projects>Open".
5. Seleccionar el "archivo Tarea_3.pro" en la carpeta del proyecto.
- Aparecerá un pop-up, ignorarlo y presionar "Ok".
6. Una vez se cargan los archivos, ingresar a la pestaña "Proyects".
7. En la sección "Proyects>Build" seleccionar la carpeta del proyecto como "Build directory".
8. En la sección "Proyects>Run" seleccionar la carpeta del proyecto como "Working directory".
9. En la sección "Proyects>Run" agregar "configurationFile.txt" (sin las comillas) al campo "Command line arguments".
10. Click a la flecha verde en la pestaña izquierda para ejecutar el programa.

#### Consideraciones:
- El programa funciona de tal manera que se pueden modificar las variables N (Number f People), I (Number of Infected People) e I_Time(Time for Recovery of Infection). Esto se puede hacer a traves de los campos de texto bajo el menu Settings.
- Settings es solo accesible si la simulacion todavia no parte o si la actual esta en pausa.
- Tener precaucion con la informacion que se entrega a los campos de texto, solo escribir numeros, ya que si se intentan ingresar otros caracteres, la simulacion ocurrira con numeros "basura" y no funcionara como debido.
- Si es que I es mas alto que N, ocurrira una simulacion "Error". En este caso, se debe pausar la simulacion e intentar con otros nímeros tales que N sea mayor que I.
- Una vez que acabe de ocupar el simulador, lo puede simplemente cerrar y despues podra revisar en el archivo de texto generado "ResultsSim.txt" todos los numeros de las simulaciones que ocurrieron la última vez que se ocupó el programa.
