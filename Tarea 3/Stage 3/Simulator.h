#ifndef SIMULATOR_H
#define SIMULATOR_H
#include "Comuna.h"
#include <ostream>
#include <fstream>
#include <QTimer>
#include <QtCharts>
#include <QChartView>
#include <QAreaSeries>
#include <QLineSeries>
class Simulator: public QObject { // By inheriting from QObject,
    //our class can use signal and slot mechanism Qt provides.
    Q_OBJECT
private:
    Comuna *comuna;
    ostream *out;
    ofstream *MyFile;
    double t;
    double speed, deltaAngle, M, delta_t, samplingTime, distanceI, prob0,prob1,prob2, tInfection;
    QTimer * timer;  // see https://doc.qt.io/qt-5.12/qtimer.html


public:
    QLineSeries *series1, *series2, *series3, *series4;
    QAreaSeries * areaSeriesSus, *areaSeriesInf, *areaSeriesRec;
    QChart* chart;
    QChartView *chartView;

    Simulator (ostream *output, Comuna *comuna,double speed, double deltaAngle,double M, double delta_t, double samplingTime, double d, double p0,double p1, double p2, double tiempoInfeccion, ofstream *Results);
    ~Simulator();
    void printStateDescription() const;
    void printState(double t) const;
    void startSimulation();
    void stopSimulation();
    double getTime();
    Comuna* getComuna();

    void createSeries();
    void updateChart();
    Simulator* getSim();

public slots:
    void simulateSlot();
};


#endif // SIMULATOR_H
