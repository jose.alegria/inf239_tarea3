#include "Simulator.h"
Simulator::Simulator(ostream *output, Comuna *com,double speed, double deltaAngle,double M,
                     double delta, double st, double d, double p0,double p1, double p2, double tiempoInfeccion, ofstream *Results){
    t=0;
    delta_t=delta;
    samplingTime=st;
    this->speed = speed;
    this->deltaAngle = deltaAngle;
    this->M = M;
    distanceI = d;
    prob0 = p0;
    prob1 = p1;
    prob2 = p2;
    tInfection = tiempoInfeccion;
    MyFile = Results;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(simulateSlot()));
    comuna = com;
    out = output;
}
Simulator::~Simulator(){
    delete timer;
}
void Simulator::printStateDescription() const {
    string s= comuna->getStateDescription();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::printState(double t) const{
    string s = to_string(t) + ",\t";
    s += comuna->getState();
    *out << s << endl;
    *MyFile << s <<endl;
}
void Simulator::startSimulation(){
    comuna->Reset(speed, deltaAngle, M);
    printStateDescription();
    t=0;
    printState(t);
    timer->start(1000);
    chart->removeAllSeries();
    Simulator::createSeries();
}
void Simulator::stopSimulation(){
    timer->stop();
}
void Simulator::simulateSlot(){
    double nextStop=t+samplingTime;
    while(t<nextStop) {
       comuna->computeNextState(delta_t,distanceI,prob0,prob1,prob2,tInfection); // compute its next state based on current global state
       comuna->updateState();  // update its state
       Simulator::updateChart();
       t+=delta_t;
    }
    printState(t);
}
Simulator* Simulator::getSim(){
    return this;
}

double Simulator::getTime(){
    return t;
}
Comuna * Simulator::getComuna(){
    return comuna;
}
void Simulator::createSeries(){
    series1 = new QLineSeries();
    series2 = new QLineSeries();
    series3 = new QLineSeries();
    series4 = new QLineSeries();

    areaSeriesSus = new QAreaSeries(series1, series2);
    areaSeriesInf = new QAreaSeries(series2,series3);
    areaSeriesRec = new QAreaSeries(series3, series4);

    chart->addSeries(areaSeriesSus);
    chart->addSeries(areaSeriesInf);
    chart->addSeries(areaSeriesRec);

    chart->setTitle("Comuna CoVid");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).first()->setRange(0, 100);
    chart->axes(Qt::Vertical).first()->setRange(0, comuna->getNumberOfPeople());
    chart->setAnimationOptions(QChart::SeriesAnimations);
}
void Simulator::updateChart(){
    *series1 << QPointF(t,0);
    *series2 << QPointF(t,comuna->getSus());
    *series3 << QPointF(t,comuna->getSus()+comuna->getInf());
    *series4 << QPointF(t,comuna->getSus()+comuna->getInf()+comuna->getRec());
    if(t>100){
        chart->axes(Qt::Horizontal).first()->setRange(0,t);
    }
}
