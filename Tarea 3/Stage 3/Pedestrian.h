#ifndef PEDESTRIAN_H
#define PEDESTRIAN_H
#include <string>
#include <QRandomGenerator>
using namespace std;
class Comuna;
class Pedestrian {
private:
    double x, y, speed, angle, deltaAngle, timerRecuperacion;
    double x_tPlusDelta, y_tPlusDelta;
    string estadoSalud, estadoSalud_tPlusDelta, mask;
    Comuna *comuna;
    QRandomGenerator myRand; // see https://doc.qt.io/qt-5/qrandomgenerator.html

public:
    Pedestrian(Comuna *com, double speed, double deltaAngle, string salud);
    static string getStateDescription() {
        return "x, \ty";
    };
    string getMask() const;
    string getState() const;
    double getX() const;
    double getY() const;
    void computeNextState(double delta_t, double d, double p0, double p1, double p2, double tiempoInfeccion);
    void updateState();
    void computeAngle();
    void computeXY(double delta_t);
    void LookforPersonInf(vector<Pedestrian*> close_person, double d, double p0, double p1, double p2);
    void startTimerRecovery(double delta_t, double tiempoInfeccion);
    void SetRandLoc();
    void setMask();
};


#endif // PEDESTRIAN_H
