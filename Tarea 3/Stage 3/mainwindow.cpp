#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setGraph(){
    QChart *chart = new QChart();
    this->chart = chart;
    this->simu->chart = chart;
    QChartView *chartView = new QChartView(chart);
    chartView->setParent(ui->horizontalWidget);
}
void MainWindow::setSim(Simulator* sim){
    simu = sim;
}
void MainWindow::startSim(){
    simu->startSimulation();
}
void MainWindow::stopSim(){
    simu->stopSimulation();
}
/*
chart->setTitle("Comuna Covid");
chart->createDefaultAxes();
chart->axes(Qt::Horizontal).first()->setRange(0, 100);
chart->axes(Qt::Vertical).first()->setRange(0, 100);
chartView->setRenderHint(QPainter::Antialiasing);
chart->setAnimationOptions(QChart::AllAnimations);*/
