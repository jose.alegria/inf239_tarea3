#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QAreaSeries>
#include <QLineSeries>
#include "Simulator.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    Simulator *simu;
    QChart* chart;
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setSim(Simulator *sim);
    void setGraph();

public slots:
    void startSim();
    void stopSim();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
